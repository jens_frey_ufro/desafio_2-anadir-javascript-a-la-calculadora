const botones = document.querySelectorAll('button');

const guardado = document.querySelector('#guardado');
const operador = document.querySelector('#operador');
const activo = document.querySelector('#activo');

botones.forEach(function(button) {
    button.addEventListener('click', calcular);
});

function calcular(event) {
  
    const valorBoton = event.target.value;

    if (valorBoton === '=') {
      
        if (guardado.value !== '') {
          
            guardado.value = eval(activo.value + operador.value + guardado.value);
            operador.value = '';
            activo.value = '';
        }
    } else {
        if (valorBoton === 'M') {
      
            activo.value *= '-1';

        } else if (valorBoton === 'C') {
          
            guardado.value = '';
            operador.value = '';
            activo.value = '';

        } else if (valorBoton === 'X') {

            var valorPrevio = activo.value;
            activo.value = valorPrevio.substr(0, valorPrevio.length - 1);

        } else if (valorBoton === '+') {

            operador.value = '+';
            guardado.value = activo.value;
            activo.value = '';
    
        } else if (valorBoton === '-') {
    
            operador.value = '-';
            guardado.value = activo.value;
            activo.value = '';
    
        } else if (valorBoton === '*') {
    
            operador.value = '*';
            guardado.value = activo.value;
            activo.value = '';
    
        } else if (valorBoton === '/') {
    
            operador.value = '/';
            guardado.value = activo.value;
            activo.value = '';
    
        } else {
          
            activo.value += valorBoton;

        }

    }

}